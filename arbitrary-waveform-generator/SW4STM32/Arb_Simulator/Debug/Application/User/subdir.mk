################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/command_decoder.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/dac.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/dma.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/generator.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/gpio.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/main.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/mystring.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/stm32f4xx_hal_msp.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/stm32f4xx_it.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/tim.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/uart_commands.c \
D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/usart.c 

OBJS += \
./Application/User/command_decoder.o \
./Application/User/dac.o \
./Application/User/dma.o \
./Application/User/generator.o \
./Application/User/gpio.o \
./Application/User/main.o \
./Application/User/mystring.o \
./Application/User/stm32f4xx_hal_msp.o \
./Application/User/stm32f4xx_it.o \
./Application/User/tim.o \
./Application/User/uart_commands.o \
./Application/User/usart.o 

C_DEPS += \
./Application/User/command_decoder.d \
./Application/User/dac.d \
./Application/User/dma.d \
./Application/User/generator.d \
./Application/User/gpio.d \
./Application/User/main.d \
./Application/User/mystring.d \
./Application/User/stm32f4xx_hal_msp.d \
./Application/User/stm32f4xx_it.d \
./Application/User/tim.d \
./Application/User/uart_commands.d \
./Application/User/usart.d 


# Each subdirectory must supply rules for building sources it contributes
Application/User/command_decoder.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/command_decoder.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/dac.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/dac.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/dma.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/dma.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/generator.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/generator.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/gpio.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/gpio.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/main.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/main.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/mystring.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/mystring.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f4xx_hal_msp.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/stm32f4xx_hal_msp.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/stm32f4xx_it.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/stm32f4xx_it.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/tim.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/tim.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/uart_commands.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/uart_commands.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Application/User/usart.o: D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Src/usart.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo %cd%
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -D__weak="__attribute__((weak))" -D__packed="__attribute__((__packed__))" -DUSE_HAL_DRIVER -DSTM32F446xx -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"D:/arbwavegen/arbitrary-waveform-generator/arbitrary-waveform-generator/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


