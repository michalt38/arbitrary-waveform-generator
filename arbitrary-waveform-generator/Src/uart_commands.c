#include "uart_commands.h"
#include "generator.h"
#include "command_decoder.h"
#include "usart.h"
#include "mystring.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "dac.h"

static char message_buffer[1024];
static char cTokenString[10];
static char received_string[512];

static void setSignalToken();
static void setAmplitudeToken();
static void setFrequencyToken();
static void setIncrementationToken();
static void writeValueToken();
static void uploadSignalToken();
static void setPSCToken();
static void setParamToken();

static void dutyCycleToken();
static void growthToken();
static void decayToken();

int fileCounter;

extern struct GeneratorVariables sGeneratorVariables;
extern unsigned char received_char;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	Reciver_PutCharacterToBuffer(received_char);
	if(READY == eReciver_GetStatus())
	{
		KeywordCode eTokenKey;

		Reciver_GetStringCopy(received_string);
		DecodeMsg(received_string);
		if(OK == eToken_GetKeywordCode(0, &eTokenKey))
		{
			switch(eTokenKey)
			{
			case SET_SIGNAL:
				setSignalToken();
				break;
			case SET_AMPL:
				setAmplitudeToken();
				break;
			case SET_FREQ:
				setFrequencyToken();
				break;
			case SET_INCREMENTATION:
				setIncrementationToken();
				break;
			case SHOW_MENU:
				sGeneratorVariables.eCommmunication_mode = MENU;
				showMenu();
				break;
			case SHOW_STATUS:
				showStatus();
				break;
			case HELP:
				showHelp();
				break;
			case STOP_DAC:
				HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
				sendOKMessage("DAC TURNED OFF");
				break;
			case START_DAC:
				HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)&sGeneratorVariables.signalArray, sGeneratorVariables.samples, DAC_ALIGN_12B_R);
				sendOKMessage("DAC TURNED ON");
				break;
			case WRITE_VALUE:
				writeValueToken();
				break;
			case UPLOAD_SIGNAL:
				uploadSignalToken();
				break;
			case UPLOAD_ABORT:
				fileCounter = 0;
				memset(sGeneratorVariables.uploadedSignal, 0, 4096);
				sendOKMessage("UPLOAD ABORTED");
				break;
			case SET_PSC:
				setPSCToken();
				break;
			case SET_PARAM:
				setParamToken();
				break;
			default:
				break;
			}
		}
		else if(MENU == sGeneratorVariables.eCommmunication_mode)
		{
			if(OK == eToken_GetString(0, cTokenString))
			{
				int choice = atoi(cTokenString);

				if(EQUAL == eCompareString(cTokenString, "^"))
				{
					showMenu();
					HAL_UART_Receive_IT(&huart2, &received_char, 1);
					return;
				}

				if(choice == 0)
				{
					sendErrorMessage("INVALID COMMAND");
					HAL_UART_Receive_IT(&huart2, &received_char, 1);
					return;
				}

				switch(choice)
				{
				case 1:
					setSignalToken();
					break;
				case 2:
					setFrequencyToken();
					break;
				case 3:
					setIncrementationToken();
					break;
				case 4:
					setAmplitudeToken();
					break;
				case 5:
					setPSCToken();
					break;
				case 6:
					showStatus();
					break;
				case 7:
					showHelp();
					break;
				case 8:
					sGeneratorVariables.eCommmunication_mode = COMMANDS;
					sendOKMessage("MENU MODE DISABLED");
					break;
				default:
					sendErrorMessage("INVALID COMMAND");
					break;
				}
			}
		}
		else
		{
			sendErrorMessage("UNKNOWN COMMAND");
		}
	}
	HAL_UART_Receive_IT(&huart2, &received_char, 1);
}

void showStatus()
{
	int len;
	char signal_str[16];

	getSignalName(sGeneratorVariables.signalType, signal_str);
	len = sprintf(message_buffer, "SIGNAL: %s\r\nFREQUENCY: %dHz\r\nINCREMENTATION: %d\r\nAMPLITUDE: %0.1fV\r\nPRESCALER: %d\r\n", signal_str, sGeneratorVariables.frequency, (int)sGeneratorVariables.dac_incrementation, sGeneratorVariables.amplitude, sGeneratorVariables.psc);
	HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
}

void showMenu()
{
	int len;

	len = sprintf(message_buffer, "1 -> SET SIGNAL\r\n2 -> SET FREQUENCY\r\n3 -> SET INCREMENTATION\r\n4 -> SET AMPLITUDE\r\n5 -> SET PRESCALER\r\n6 -> SHOW STATUS\r\n7 -> HELP\r\n8 -> EXIT MENU\r\n");
	HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
}

void showHelp()
{
	int len;

	len = sprintf(message_buffer, "AVAILABLE COMMANDS:\r\n>>> SHOW_MENU\r\n>>> SHOW_STATUS\r\n>>> SET_SIGNAL\r\n>>> SET_PARAM\r\nSET_FREQ\r\n>>> SET_INCR\r\n>>> SET_AMPL\r\n>>> SET_PSC\r\n"
			">>> START_DAC\r\n>>> STOP_DAC\r\n>>> WRITE_VALUE\r\n>>> UPLOAD_SIGNAL\r\n>>> UPLOAD_ABORT\r\nAVAILABLE SIGNALS:\r\n>>> NO_SIGNAL\r\n>>> SINE\r\n>>> SQUARE\r\n>>> TRIANGLE\r\n"
			">>> SAWTOOTH\r\n>>> SINC\r\n>>> GAUSSIAN\r\n>>> EXPONENTIAL\r\n>>> DC\r\n");
	HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
}

static void setPSCToken()
{
	memset(cTokenString, 0, 10);

	if(OK == eToken_GetString(1, cTokenString))
	{
		int psc = atoi(cTokenString);

		if(psc != 0)
		{
			setPSC(psc);
			sendOKMessage("PRESCALER SET TO %d", psc);
		}
		else
			sendErrorMessage("INVALID PARAMETER");
	}
	else
	{
		sendErrorMessage("UNKNOWN VALUE");
	}
}

static void uploadSignalToken()
{
	char values_str[496];
	char value[5];

	memset(values_str, 0, 496);
	memset(value, 0, 5);

	if(OK == eToken_GetString(1, values_str))
	{
		int value_counter = 0;
		for(int i = 0; ; i++)
		{
			if(values_str[i] != ',' && values_str[i] != (int)NULL)
			{
				value[value_counter++] = values_str[i];
			}
			else
			{
				sGeneratorVariables.uploadedSignal[fileCounter++] = atoi(value);
				value_counter = 0;
				memset(value, (int)NULL, 5);
				if(values_str[i] == (int)NULL)
					break;
			}
		}
	}
	else
		sendOKMessage("INVALID PARAMETER");

	if(fileCounter < 4096)
		sendOKMessage("%d VALUES WRITTEN", fileCounter);
	else if(fileCounter == 4096)
	{
		sendOKMessage("%d VALUES WRITTEN", fileCounter);
		fileCounter = 0;
		sGeneratorVariables.signalType = ARBITRARY;
		setSignal(ARBITRARY);
	}
	else
	{
		sendErrorMessage("TOO MUCH PROBES");
		fileCounter = 0;
	}
}

static void writeValueToken()
{
	int index, value;

	memset(cTokenString, 0, 10);

	if(OK == eToken_GetString(1, cTokenString))
	{
		index = atoi(cTokenString);

		if(index < 0 || index > sGeneratorVariables.samples)
		{
			sendErrorMessage("INVALID INDEX");
			return;
		}
	}
	else
	{
		sendErrorMessage("INVALID PARAMETER");
		return;
	}

	memset(cTokenString, 0, 10);

	if(OK == eToken_GetString(2, cTokenString))
	{
		value = atoi(cTokenString);

		if(value < 0 || value > 4095)
		{
			sendErrorMessage("INVALID VALUE");
			return;
		}

		sGeneratorVariables.signalArray[index] = value;
		sendOKMessage("VALUE SET TO %d", value);
	}
	else
	{
		sendErrorMessage("INVALID PARAMETER");
	}

	sGeneratorVariables.signalType = ARBITRARY;
}

static void setParamToken()
{
	KeywordCode eTokenKey;

	if(OK == eToken_GetKeywordCode(1, &eTokenKey))
		switch(eTokenKey)
		{
		case DUTY_CYCLE:
			dutyCycleToken();
			break;
		case GROWTH:
			growthToken();
			break;
		case DECAY:
			decayToken();
			break;
		default:
			sendErrorMessage("UNKNOWN PARAMETER");
			break;
		}
	else
		sendErrorMessage("UNDEFINED PARAMETER");
}

static void setAmplitudeToken()
{
	memset(cTokenString, 0, 10);

	if(OK == eToken_GetString(1, cTokenString))
	{
		double ampl = atof(cTokenString);

		if(ampl != 0)
		{
			if(OK == setAmplitude(ampl))
				sendOKMessage("AMPLITUDE SET TO %0.1fV", ampl);
			else
				sendErrorMessage("OUT OF RANGE");
		}
		else
			sendErrorMessage("INVALID PARAMETER");
	}
	else
		sendErrorMessage("UNKNOWN VALUE");
}

static void setFrequencyToken()
{
	memset(cTokenString, 0, 10);

	if(OK == eToken_GetString(1, cTokenString))
	{
		int freq = atoi(cTokenString);

		if(freq != 0)
		{
			setFrequency(freq);
			sendOKMessage("FREQUENCY SET TO %dHz", sGeneratorVariables.frequency);
		}
		else
			sendErrorMessage("INVALID PARAMETER");
	}
	else
		sendErrorMessage("UNKNOWN VALUE");
}

static void setIncrementationToken()
{
	memset(cTokenString, 0, 10);

	if(OK == eToken_GetString(1, cTokenString))
	{
		int incr = atoi(cTokenString);

		if(incr != 0)
		{
			setIncrementation(incr);
			sendOKMessage("INCREMENTATION SET TO %d", incr);
		}
		else
			sendErrorMessage("INVALID PARAMETER");
	}
	else
	{
		sendErrorMessage("UNKNOWN VALUE");
	}
}

static void setSignalToken()
{
	KeywordCode eTokenKey;

	if(OK == eToken_GetKeywordCode(1, &eTokenKey))
	{
		char signal_name[10];

		if((Signal)eTokenKey == NO_SIGNAL)
		{
			sGeneratorVariables.signalType = NO_SIGNAL;
			sendOKMessage("NO SIGNAL SET");
			return;
		}

		setSignal(eTokenKey);
		getSignalName(eTokenKey, signal_name);
		sendOKMessage("SIGNAL SET TO %s", signal_name);
	}
	else
		sendErrorMessage("UNKNOWN SIGNAL");
}

static void dutyCycleToken()
{
	if(sGeneratorVariables.signalType == SQUARE || sGeneratorVariables.signalType == SINC || sGeneratorVariables.signalType == GAUSSIAN
			|| sGeneratorVariables.signalType == SAWTOOTH)
	{
		memset(cTokenString, 0, 10);

		if(OK == eToken_GetString(2, cTokenString))
		{
			double cycle = atof(cTokenString)/100;

			if(cycle < 0 || cycle > 1)
			{
				sendErrorMessage("INVALID PARAMETER");
				return;
			}


			if(sGeneratorVariables.signalType == SINC || sGeneratorVariables.signalType == GAUSSIAN)
			{
				if((1-cycle) != 0)
					sGeneratorVariables.sSignalParameters.duty_cycle = cycle;
				else
					sGeneratorVariables.sSignalParameters.duty_cycle = 1;
			}
			else
				sGeneratorVariables.sSignalParameters.duty_cycle = cycle;
			sendOKMessage("DUTY CYCLE SET TO %s%", cTokenString);

			setSignal(sGeneratorVariables.signalType);
		}
		else
		{
			sendErrorMessage("UNKNOWN VALUE");
		}
	}
	else
		sendErrorMessage("PARAMETER UNAVAILABLE FOR CURRENT SIGNAL");
}

static void growthToken()
{
	if(sGeneratorVariables.signalType == EXPONENTIAL)
	{
		sGeneratorVariables.sSignalParameters.growth = true;
		setSignal(EXPONENTIAL);
		sendOKMessage("SIGNAL TYPE SET TO GROWTH");
	}
	else
		sendErrorMessage("PARAMETER UNAVAILABLE FOR CURRENT SIGNAL");
}

static void decayToken()
{
	if(sGeneratorVariables.signalType == EXPONENTIAL)
	{
		sGeneratorVariables.sSignalParameters.growth = false;
		setSignal(EXPONENTIAL);
		sendOKMessage("SIGNAL TYPE SET TO DECAY");
	}
	else
		sendErrorMessage("PARAMETER UNAVAILABLE FOR CURRENT SIGNAL");
}

void sendErrorMessage(char message[])
{
	int len;

	len = sprintf(message_buffer, "ERR %s\r\n", message);
	HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
}

void sendOKMessage(char message[], ...)
{
	int len;
	va_list list;
	char msg[strlen(message)+5];
	msg[0] = 'O'; msg[1] = 'K'; msg[2] = ' '; msg[3] = (int)NULL;
	AppendString(message, msg);
	msg[strlen(message)+3] = '\r';
	msg[strlen(message)+4] = '\n';
	msg[strlen(message)+5] = (int)NULL;

	va_start(list, message);

	for(int counter = 0; msg[counter] != (int)NULL; counter++)
	{
		if(msg[counter] == '%')
		{
			switch(msg[counter+1])
			{
			case 'd':
				len = sprintf(message_buffer, msg, va_arg(list, int));
				HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
				return;
			case 'f':
				len = sprintf(message_buffer, msg, va_arg(list, double));
				HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
				return;
			case 's':
				len = sprintf(message_buffer, msg, va_arg(list, char*));
				HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
				return;
			}
			if(msg[counter+4] == 'f')
			{
				len = sprintf(message_buffer, msg, va_arg(list, double));
				HAL_UART_Transmit_IT(&huart2, (uint8_t*)message_buffer, len);
				return;
			}
		}
	}

	HAL_UART_Transmit_IT(&huart2, (uint8_t*)msg, strlen(msg));
}
