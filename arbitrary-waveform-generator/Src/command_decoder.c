#include "command_decoder.h"
#include "mystring.h"
#include <string.h>
#include <stdio.h>

#define MAX_KEYWORD_STRING_LTH 13
#define MAX_KEYWORD_NR 28
#define MAX_TOKEN_NR 3
#define SPACE 32

typedef enum TokenType {KEYWORD, NUMBER, STRING} TokenType;
unsigned char ucTokenNr;

typedef union TokenValue
{
    KeywordCode eKeyword;
    unsigned int uiNumber;
    char pcString[256];
} TokenValue;

typedef struct Keyword
{
    KeywordCode eCode;
    char cString[MAX_KEYWORD_STRING_LTH + 1];
} Keyword;

struct Keyword asKeywordList[MAX_KEYWORD_NR] =
{
    {SET_SIGNAL, "SET_SIGNAL"},
    {SET_FREQ, "SET_FREQ"},
	{SET_AMPL, "SET_AMPL"},
	{SET_INCREMENTATION, "SET_INCR"},
	{NO_SIGNAL, "NO_SIGNAL"},
	{SINE, "SINE"},
	{SQUARE, "SQUARE"},
	{TRIANGLE, "TRIANGLE"},
	{SAWTOOTH, "SAWTOOTH"},
	{SINC, "SINC"},
	{DC, "DC"},
	{GAUSSIAN, "GAUSSIAN"},
	{EXPONENTIAL, "EXPONENTIAL"},
	{ARBITRARY, "ARBITRARY"},
	{SHOW_MENU, "SHOW_MENU"},
	{SHOW_STATUS, "SHOW_STATUS"},
	{HELP, "HELP"},
	{STOP_DAC, "STOP_DAC"},
	{START_DAC, "START_DAC"},
	{WRITE_VALUE, "WRITE_VALUE"},
	{UPLOAD_SIGNAL, "UPLOAD_SIGNAL"},
	{UPLOAD_ABORT, "UPLOAD_ABORT"},
	{SET_PSC, "SET_PSC"},
	{DUTY_CYCLE, "DUTY_CYCLE"},
	{SET_PARAM, "SET_PARAM"},
	{GROWTH, "GROWTH"},
	{DECAY, "DECAY"},
	{FACTOR, "FACTOR"}
};

typedef struct Token
{
    TokenType eType;
    TokenValue uValue;
} Token;

Token asToken[MAX_TOKEN_NR];

typedef enum CharState {DELIMITER, TOKEN} CharState;

enum Result eToken_GetKeywordCode(unsigned char ucIndex, KeywordCode* eKey)
{
	if((0 != ucTokenNr) && (KEYWORD == asToken[ucIndex].eType))
	{
		*eKey = asToken[ucIndex].uValue.eKeyword;
		return OK;
	}
	else
		return ERR;
}

enum Result eToken_GetNumber(unsigned char ucIndex, unsigned int* uiValue)
{
	if((0 != ucTokenNr) && (NUMBER == asToken[ucIndex].eType))
	{
		*uiValue = asToken[ucIndex].uValue.uiNumber;
		return OK;
	}
	else
		return ERR;
}

enum Result eToken_GetString(unsigned char ucIndex, char* ucDestination)
{
	if((0 != ucTokenNr) && (STRING == asToken[ucIndex].eType))
	{
		CopyString(asToken[ucIndex].uValue.pcString, ucDestination);
		memset(asToken[ucIndex].uValue.pcString, 0, strlen(asToken[ucIndex].uValue.pcString));
		return OK;
	}
	else
		return ERR;
}

unsigned char ucFindTokensInString(char *pcString)
{
    char *cTokens = pcString;
    unsigned char ucCharacterCounter;
    unsigned char ucTokenAmount = 0;
    CharState eCharState = TOKEN;
    CharState ePreviousCharState = DELIMITER;

    for(ucCharacterCounter = 0; ; ucCharacterCounter++)
    {
        switch(eCharState)
        {
        case TOKEN:
            if((MAX_TOKEN_NR <= ucTokenNr) || ((int)NULL == cTokens[ucCharacterCounter]))
            {
                return ucTokenAmount;
            }
            else if(SPACE == cTokens[ucCharacterCounter])
            {
                eCharState = DELIMITER;
                ePreviousCharState = TOKEN;
            }
            else if(DELIMITER == ePreviousCharState)
            {
            	CopyString(&cTokens[ucCharacterCounter], asToken[ucTokenAmount++].uValue.pcString);
                ePreviousCharState = TOKEN;
            }
            break;
        case DELIMITER:
            if((int)NULL == cTokens[ucCharacterCounter])
            {
                return ucTokenAmount;
            }
            else if(SPACE != cTokens[ucCharacterCounter])
            {
                eCharState = TOKEN;
                ePreviousCharState = DELIMITER;
                ucCharacterCounter--;
            }
            break;
        }
    }
}

enum Result eStringKeyword(char pcStr[], enum KeywordCode *peKeywordCode)
{
    unsigned char ucKeywordNr;

    for(ucKeywordNr = 0; MAX_KEYWORD_NR > ucKeywordNr; ucKeywordNr++)
    {
        if(EQUAL == eCompareString(pcStr, asKeywordList[ucKeywordNr].cString))
        {
            *peKeywordCode = asKeywordList[ucKeywordNr].eCode;
            return OK;
        }
    }
    return ERR;
};

void DecodeTokens()
{
    unsigned char ucTokenCounter;
    unsigned int uiHexValue;
    KeywordCode eKeyValue;

    for(ucTokenCounter = 0; ucTokenCounter < ucTokenNr; ucTokenCounter++)
    {
        if(OK == eStringKeyword(asToken[ucTokenCounter].uValue.pcString, &eKeyValue))
        {
            asToken[ucTokenCounter].uValue.eKeyword = eKeyValue;
            asToken[ucTokenCounter].eType = KEYWORD;
        }
        else if(OK == eHexStringToUInt(asToken[ucTokenCounter].uValue.pcString, &uiHexValue))
        {
            asToken[ucTokenCounter].uValue.uiNumber = uiHexValue;
            asToken[ucTokenCounter].eType = NUMBER;
        }
        else
        {
            asToken[ucTokenCounter].eType = STRING;
        }
    }
}

void DecodeMsg(char *pcString)
{
	for(unsigned char ucIndex = 0; ucIndex < MAX_TOKEN_NR; ucIndex++)
	{
		asToken[ucIndex].eType = STRING;
	}

    unsigned char ucTokenCounter;

    ucTokenNr = ucFindTokensInString(pcString);
    for(ucTokenCounter = 0; ucTokenNr > ucTokenCounter; ucTokenCounter++)
    {
        ReplaceCharactersInString(asToken[ucTokenCounter].uValue.pcString, SPACE, (int)NULL);
    }
    DecodeTokens();
}
