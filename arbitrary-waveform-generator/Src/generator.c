#include "generator.h"
#include "dac.h"
#include "usart.h"
#include "mystring.h"
#include <stdarg.h>
#include <stdlib.h>
#include "uart_commands.h"
#include <stdio.h>
#include <string.h>

int __errno;

#include "math.h"

#define M_PI 3.14
#define SWAP(a, b) do { typeof(a) temp = a; a = b; b = temp; } while (0)

struct GeneratorVariables sGeneratorVariables;

void makeSineWave(uint16_t* array, int numberOfSamples)
{
	for(int counter = 0; counter < numberOfSamples; counter++)
	{
			array[counter] = (uint16_t)((((sin((double)(counter*2*M_PI)/numberOfSamples)+1)*((DAC_MAX_VALUE+1)/2))
					*sGeneratorVariables.amplitude/V_REF) + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	}
}

void makeSquareWave(uint16_t* array, int numberOfSamples)
{
	for(int counter = 0; counter < numberOfSamples; counter++)
	{
		array[counter] = (uint16_t)(((counter < ((numberOfSamples+1)*(1 - sGeneratorVariables.sSignalParameters.duty_cycle))?0:DAC_MAX_VALUE)
				*sGeneratorVariables.amplitude/V_REF) + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	}
}

void makeTriangularWave(uint16_t* array, int numberOfSamples)
{
	for(int counter = 0; counter < numberOfSamples; counter++)
	{
		array[counter] = (uint16_t)(((counter < (numberOfSamples+1)/2?
				counter*((DAC_MAX_VALUE+1)/numberOfSamples):DAC_MAX_VALUE-(counter*((DAC_MAX_VALUE+1)/numberOfSamples)))
				*sGeneratorVariables.amplitude/V_REF)*2 + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	}
}

void makeSawtoothWave(uint16_t* array, int numberOfSamples)
{
	double factor = sGeneratorVariables.sSignalParameters.duty_cycle;
	double value;
	double scale;
	double tg;
	bool reverse = false;

	if(factor < 0.5)
	{
		factor = 1 - factor;
		reverse = true;
	}

	for(int counter = 0; counter < numberOfSamples*factor; counter++)
	{
		array[counter] = (uint16_t)(counter*factor*10);
	}

	value = array[(int)(numberOfSamples*factor) - 1];

	tg = value/(numberOfSamples*(1-factor));

	for(int counter = numberOfSamples*factor; counter < numberOfSamples; counter++)
	{
		array[counter] = (uint16_t)(array[(int)(numberOfSamples*factor) - 1] - (counter-numberOfSamples*factor)*(tg));
	}

	scale = 1/value;

	for(int counter = 0; counter < numberOfSamples; counter++)
	{
		array[counter] = (uint16_t)(((((double)array[counter]*scale)*DAC_MAX_VALUE)
				*sGeneratorVariables.amplitude/V_REF) + (((V_REF-sGeneratorVariables.amplitude)/2))*(DAC_MAX_VALUE/V_REF));
	}

	if(reverse)
	{
		for(int counter = 0; counter < numberOfSamples/2; counter++)
		{
			SWAP(array[counter], array[numberOfSamples - counter - 1]);
		}
	}
}

void makeSincWave(uint16_t* array, int numberOfSamples)
{
	double _min = sin((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)*((-numberOfSamples/2)*2*3.14)/numberOfSamples)/((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)*((-numberOfSamples/2)*2*3.14)/numberOfSamples);

	for(int counter = (-numberOfSamples/2) + 1; counter < numberOfSamples/2; counter++)
	{
	    if(counter == 0) continue;

		if((sin((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)*(counter*2*3.14)/numberOfSamples)/((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)*(counter*2*3.14)/numberOfSamples) < _min))
			_min = sin((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)*(counter*2*3.14)/numberOfSamples)/((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)*(counter*2*3.14)/numberOfSamples);
	}

	for(int counter = -(numberOfSamples/2); counter < numberOfSamples/2; counter++)
	{
		if(counter != 0)
			array[counter + numberOfSamples/2] = (uint16_t)((((sin((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)
					*(counter*2*M_PI)/numberOfSamples)/((double)140*(1-sGeneratorVariables.sSignalParameters.duty_cycle)
							*(counter*2*M_PI)/numberOfSamples)) - _min)
								*DAC_MAX_VALUE*((double)1/((double)1 - _min))*sGeneratorVariables.amplitude/V_REF)
									+ ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
		else
			array[numberOfSamples/2] = (uint16_t)((DAC_MAX_VALUE
					*sGeneratorVariables.amplitude/V_REF) + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	}
}

void  makeExponentialWave(uint16_t* array, int numberOfSamples)
{
	double e;
	double prescaler = 1000;
	int value;

	if(sGeneratorVariables.sSignalParameters.growth)
	{
		for(int counter = 0; counter < numberOfSamples; counter++)
		{
			if(((double)counter/prescaler) > 11)
				e = 1;
			else
				e = exp((double)(counter/prescaler))/exp((numberOfSamples-1)/prescaler);

			array[counter] = (uint16_t)(e*DAC_MAX_VALUE);
		}

		value = array[0];

		for(int counter = 0; counter < numberOfSamples; counter++)
		{
			array[counter] -= value;
		}

		value = array[numberOfSamples - 1];

		for(int counter = 0; counter < numberOfSamples; counter++)
		{
			array[counter] *= (DAC_MAX_VALUE/value);
		}
	}
	else
	{
		for(int counter = -numberOfSamples; counter < 0; counter++)
		{
			if(((double)((-counter)/prescaler)) < -11)
				e = 0;
			else
				e = exp((double)(-(counter/prescaler)))/exp(-(-numberOfSamples/prescaler));

			array[counter+numberOfSamples] = (uint16_t)(e*DAC_MAX_VALUE);
		}

		value = array[numberOfSamples - 1];

		for(int counter = 0; counter < numberOfSamples; counter++)
		{
			array[counter] -= value;
		}

		value = array[0];

		for(int counter = 0; counter < numberOfSamples; counter++)
		{
			array[counter] *= (DAC_MAX_VALUE/value);
		}
	}

	for(int counter = 0; counter < numberOfSamples; counter++)
	{
		array[counter] = ((array[counter]*sGeneratorVariables.amplitude/V_REF) + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	}
}

void makeGaussianWave(uint16_t* array, int numberOfSamples)
{
	double variance = 0.16;
 	double a = 1/(sqrt((variance*2*M_PI)));
 	double b = 0; //expected value
 	double c = sqrt(variance);
	double e;
	double p;

 	for(int counter = -(numberOfSamples/2); counter < numberOfSamples/2; counter++)
 	{
		p = (-pow(((counter*(1-sGeneratorVariables.sSignalParameters.duty_cycle)/25)-b),2))/(2*pow(c,2));
		if(p < -11)
			e = 0;
		else
			e = exp(p);
		array[counter + numberOfSamples/2] = (uint16_t)((a*e)*(DAC_MAX_VALUE
				*(sGeneratorVariables.amplitude/V_REF)) + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	 }
}

void makeDCWave(uint16_t* array, int numberOfSamples)
{
	for(int counter = 0; counter < numberOfSamples; counter++)
	{
		array[counter] = (uint16_t)(DAC_MAX_VALUE*sGeneratorVariables.amplitude/V_REF);
	}
}


void modifyUploadedWave(uint16_t* array, int numberOfSamples)
{
	for(int counter = 0; counter < numberOfSamples; counter++)
	{
		array[counter] = (uint16_t)((sGeneratorVariables.uploadedSignal[counter*sGeneratorVariables.dac_incrementation]
				*sGeneratorVariables.amplitude/V_REF) + ((V_REF-sGeneratorVariables.amplitude)/2)*(DAC_MAX_VALUE/V_REF));
	}
}

void setSignalParametersToDefault()
{
	sGeneratorVariables.sSignalParameters.duty_cycle = 0.5;
	sGeneratorVariables.sSignalParameters.growth = true;
}

void setFrequency(int freq)
{
	if(0 == freq%BASE_FREQ)
	{
		setIncrementation(freq/BASE_FREQ);
		TIM8->PSC = MIN_PRESCALER;
		sGeneratorVariables.psc = MIN_PRESCALER;
	}
	else
	{
		TIM8->PSC = MIN_PRESCALER;
		sGeneratorVariables.psc = MIN_PRESCALER;
		setIncrementation((freq/BASE_FREQ)+1);
		sGeneratorVariables.psc = (MIN_PRESCALER*sGeneratorVariables.frequency)/freq + 1;
		sGeneratorVariables.frequency = freq;
		TIM8->PSC = sGeneratorVariables.psc;
	}
}

enum Result setAmplitude(double amplitude)
{
	if(amplitude > V_REF || amplitude <= 0) return ERR;

	sGeneratorVariables.amplitude = amplitude;
	if(sGeneratorVariables.signalType != NO_SIGNAL)
		setSignal(sGeneratorVariables.signalType);
	return OK;
}

void setPSC(int psc)
{
	TIM8->PSC = psc;
	sGeneratorVariables.frequency = (MIN_PRESCALER*sGeneratorVariables.dac_incrementation*BASE_FREQ)/psc;
	sGeneratorVariables.psc = psc;
}

void setIncrementation(uint32_t incr)
{
	sGeneratorVariables.dac_incrementation = incr;
	sGeneratorVariables.frequency = (MIN_PRESCALER*incr*BASE_FREQ)/sGeneratorVariables.psc;
	sGeneratorVariables.samples = 4096/sGeneratorVariables.dac_incrementation;
	setSignal(sGeneratorVariables.signalType);
}

void setSignal(Signal eSignal)
{
	bool unknown = false;

	if(eSignal != sGeneratorVariables.signalType)
		setSignalParametersToDefault();

	memset(sGeneratorVariables.signalArray, 0, 4096);

	switch(eSignal)
	{
	case SINE:
		makeSineWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = SINE;
		break;
	case SQUARE:
		makeSquareWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = SQUARE;
		break;
	case TRIANGLE:
		makeTriangularWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = TRIANGLE;
		break;
	case SAWTOOTH:
		if(sGeneratorVariables.signalType != SAWTOOTH)
			sGeneratorVariables.sSignalParameters.duty_cycle = 1;
		makeSawtoothWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = SAWTOOTH;
		break;
	case DC:
		makeDCWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = DC;
		break;
	case SINC:
		makeSincWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = SINC;
		break;
	case GAUSSIAN:
		makeGaussianWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = GAUSSIAN;
		break;
	case EXPONENTIAL:
		makeExponentialWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = EXPONENTIAL;
		break;
	case ARBITRARY:
		modifyUploadedWave(sGeneratorVariables.signalArray, sGeneratorVariables.samples);
		sGeneratorVariables.signalType = ARBITRARY;
		break;
	case NO_SIGNAL:
		sGeneratorVariables.signalType = NO_SIGNAL;
		break;
	default:
		sendErrorMessage("UNKNOWN SIGNAL");
		unknown = true;
		break;
	}

	if(eSignal == NO_SIGNAL)
		HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
	else if(!unknown)
	{
		HAL_DAC_Stop_DMA(&hdac, DAC_CHANNEL_1);
		HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)&sGeneratorVariables.signalArray, sGeneratorVariables.samples, DAC_ALIGN_12B_R);
	}
}

void getSignalName(Signal eSignal, char *cSignal)
{
	switch(eSignal)
	{
	case NO_SIGNAL:
		CopyString("NO_SIGNAL", cSignal);
		break;
	case SINE:
		CopyString("SINE", cSignal);
		break;
	case SQUARE:
		CopyString("SQUARE", cSignal);
		break;
	case TRIANGLE:
		CopyString("TRIANGLE", cSignal);
		break;
	case SAWTOOTH:
		CopyString("SAWTOOTH", cSignal);
		break;
	case DC:
		CopyString("DC", cSignal);
		break;
	case SINC:
		CopyString("SINC", cSignal);
		break;
	case GAUSSIAN:
		CopyString("GAUSSIAN", cSignal);
		break;
	case EXPONENTIAL:
		CopyString("EXPONENTIAL", cSignal);
		break;
	case ARBITRARY:
		CopyString("ARBITRARY", cSignal);
		break;
	default:
		CopyString("UNKNOWN", cSignal);
		break;
	}
}
