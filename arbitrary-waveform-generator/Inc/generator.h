#ifndef __generator_H
#define __generator_H

#include <stdint.h>
#include "command_decoder.h"

#define BASE_FREQ 743
#define V_REF 3.3
#define DAC_MAX_VALUE 0xFFF
#define MIN_PRESCALER 29

typedef enum Signal Signal;
typedef enum communication_mode {COMMANDS, MENU} communication_mode;
typedef enum bool {false, true} bool;

struct signalParameters{
	double duty_cycle;
	bool growth;
};

struct GeneratorVariables{
	enum Signal signalType;
	int samples;
	unsigned int psc;
	int frequency;
	uint16_t signalArray[4096];
	uint16_t uploadedSignal[4096];
	double amplitude;
	uint32_t dac_incrementation;
	communication_mode eCommmunication_mode;
	struct signalParameters sSignalParameters;
};

void makeSineWave(uint16_t* array, int numberOfSamples);
void makeSquareWave(uint16_t* array, int numberOfSamples);
void makeTriangularWave(uint16_t* array, int numberOfSamples);
void makeSawtoothWave(uint16_t* array, int numberOfSamples);
void makeDCWave(uint16_t* array, int numberOfSamples);
void makeSincWave(uint16_t* array, int numberOfSamples);
void makeGaussianWave(uint16_t* array, int numberOfSamples);
void makeExponentialWave(uint16_t* array, int numberOfSamples);
void modifyUploadedWave(uint16_t* array, int numberOfSamples);

void setSignalParametersToDefault();
void setFrequency(int freq);
enum Result setAmplitude(double amplitude);
void setSignal(Signal eSignal);
void setIncrementation(uint32_t incr);
void getSignalName(Signal eSignal, char *cSignal);
void setPSC(int psc);

#endif
