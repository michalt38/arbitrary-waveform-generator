#ifndef __command_decoder_H
#define __command_decoder_H

typedef enum KeywordCode {SET_SIGNAL, SET_FREQ, SET_AMPL, SET_INCREMENTATION, SHOW_MENU, SHOW_STATUS, HELP, STOP_DAC, START_DAC, WRITE_VALUE, UPLOAD_SIGNAL, UPLOAD_ABORT, SET_PSC, SET_PARAM} KeywordCode;
typedef enum Signal {NO_SIGNAL, SINE, SQUARE, TRIANGLE, SAWTOOTH, SINC, DC, GAUSSIAN, EXPONENTIAL, ARBITRARY} Signal;
typedef enum SignalParameter {DUTY_CYCLE, GROWTH, DECAY, FACTOR} SignalParameter;
enum Result {OK, ERR};

unsigned char ucFindTokensInString(char *);
enum Result eStringKeyword(char [], KeywordCode *);
void DecodeTokens(void);
void DecodeMsg(char *);
enum Result eToken_GetKeywordCode(unsigned char, KeywordCode*);
enum Result eToken_GetNumber(unsigned char, unsigned int*);
enum Result eToken_GetString(unsigned char ucIndex, char* ucDestination);
#endif
