#ifndef __uart_commands_H
#define __uart_commands_H

void showStatus();
void showMenu();
void showHelp();
void sendErrorMessage(char message[]);
void sendOKMessage(char message[], ...);

#endif
